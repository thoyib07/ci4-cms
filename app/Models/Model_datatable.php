<?php

namespace App\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');


class Model_datatable
{
    protected $db;
    protected $builder;
    public function __construct()
    {
        $this->db      = \Config\Database::connect();
    }

    private function _get_datatables_query($type = null, $id = null)
    {
        switch ($type) {
            case 'user':
            case 'del_user':
                $column_order = array('p.id_user', 'p.nama_user', 'p.email', 'r.nama_role'); //set column field database for datatable orderable
                $column_search = array('p.nama_user', 'p.email', 'r.nama_role'); //set column field database for datatable searchable
                $order = array('p.id_user' => 'desc'); // default order

                $this->builder = $this->db->table("m_user as p");
                $this->builder->select("p.*,r.nama_role");
                $this->builder->join("m_role as r", "r.id_role = p.role and r.status = 1");
                break;
            case 'role':
            case 'del_role':
                $column_order = array('p.id_role', 'p.nama_role'); //set column field database for datatable orderable
                $column_search = array('p.nama_role'); //set column field database for datatable searchable
                $order = array('p.id_role' => 'desc'); // default order

                $this->builder = $this->db->table("m_role as p");
                $this->builder->select("p.*");
                break;

            default:
                # code...
                break;
        }

        $i = 0;
        foreach ($column_search as $item) // loop column 
        {
            if (isset($_POST['search'])) {
                if ($_POST['search']['value']) // if datatable send POST for search
                {
                    if ($i === 0) // first loop
                    {
                        $this->builder->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->builder->like($item, $_POST['search']['value']);
                    } else {
                        $this->builder->or_like($item, $_POST['search']['value']);
                    }

                    if (count($column_search) - 1 == $i) //last loop
                        $this->builder->group_end(); //close bracket
                }
                $i++;
            }
        }

        if (!isset($order_req)) {
            if (isset($_POST['order'])) // here order processing
            {
                $this->builder->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            } else if (isset($order)) {
                foreach ($order as $key => $value) {
                    $this->builder->order_by($key, $value);
                }
            }
        }
    }

    public function get_datatables($type = null, $id = null)
    {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->builder->where("p.status = 1");
                break;
            case 'del_user':
                $this->builder->where("p.status = 0");
                break;

            case 'role':
                $this->builder->where("p.status = 1");
                break;
            case 'del_role':
                $this->builder->where("p.status = 0");
                break;

            default:
                # code...
                break;
        }

        if ($_POST['length'] != -1) {
            $this->builder->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->builder->get();
        return $query->result_array();
    }

    public function count_filtered($type = null, $id = null)
    {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->builder->where("p.status = 1");
                break;
            case 'del_user':
                $this->builder->where("p.status = 0");
                break;

            case 'role':
                $this->builder->where("p.status = 1");
                break;
            case 'del_role':
                $this->builder->where("p.status = 0");
                break;

            default:
                # code...
                break;
        }

        $query = $this->builder->get();
        return $query->num_rows();
    }

    public function count_all($type = null, $id = null)
    {
        $this->_get_datatables_query($type);

        switch ($type) {
            case 'user':
                $this->builder->where("p.status = 1");
                break;
            case 'del_user':
                $this->builder->where("p.status = 0");
                break;

            case 'role':
                $this->builder->where("p.status = 1");
                break;
            case 'del_role':
                $this->builder->where("p.status = 0");
                break;

            default:
                # code...
                break;
        }

        return $this->builder->count_all_results();
    }
}
