<?php

namespace App\Models;

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_global
{

    protected $db;
    protected $builder;
    public function __construct()
    {
        $this->db      = \Config\Database::connect();
    }

    public function getDataQuery($querySQL)
    {
        $query = $this->db->query($querySQL);
        return $query->getResult();
    }

    public function _getData($data)
    {
        if (isset($data['table'])) {
            $this->builder = $this->db->table($data['table']);
        } elseif (isset($data['from'])) {
            $this->builder = $this->db->table($data['from']);
        }

        $this->builder->select($data['select']);

        if (isset($data['where'])) {
            if (is_array($data['where'])) {
                foreach ($data['where'] as $where) {
                    if (is_array($where)) {
                        $this->builder->where($where['0'], $where['1']);
                    } else {
                        $this->builder->where($where);
                    }
                }
            } else {
                $this->builder->where($data['where']);
            }
        }

        if (isset($data['join'])) {
            foreach ($data['join'] as $join) {
                if (!empty($join['2'])) {
                    $this->builder->join($join['0'], $join['1'], $join['2']);
                } else {
                    $this->builder->join($join['0'], $join['1']);
                }
            }
        }

        if (isset($data['limit'])) {
            if (is_array($data['limit'])) {
                $this->builder->limit($data['limit']['0'], $data['limit']['1']);
            } else {
                $this->builder->limit($data['limit']);
            }
        }

        if (isset($data['group_by'])) {
            $this->builder->group_by($data['group_by']);
        }

        if (isset($data['order'])) {
            foreach ($data['order'] as $order) {
                $this->builder->order_by($order['0'], $order['1']);
            }
        }
    }

    public function getData($data)
    {
        $this->_getData($data);
        return $this->builder->getResult();
    }

    public function getDataRows($data)
    {
        $this->_getData($data);
        return $this->builder->getNumRows();
    }

    public function addData($value = null)
    {
        $this->builder = $this->db->table($value['table']);
        $this->builder->insert($value['data']);
        $id = $this->db->insertID();
        // Set Log
        return $id;
    }

    public function updateData($value)
    {
        $this->builder = $this->db->table($value['table']);
        if (isset($value['where'])) {
            if (is_array($value['where'])) {
                foreach ($value['where'] as $where) {
                    $this->builder->where($where['0'], $where['1']);
                }
            } else {
                $this->builder->where($value['where']);
            }
        }
        $this->builder->set($value['data']);
        $this->builder->update();
        // Set Log
        return ($this->db->affectedRows() > 0) ? TRUE : FALSE;
    }

    public function delById($value)
    {
        $this->builder = $this->db->table($value['table']);
        $data = array('status' => '0',);

        if (isset($value['field'])) {
            $this->builder->where($value['field'], $value['id']);
        } else {
            $this->builder->where($value['search'], $value['id']);
        }

        $this->builder->set($data);
        $this->builder->update();
        // Set Log
        return ($this->db->affectedRows() > 0) ? TRUE : FALSE;
    }

    public function restorById($value)
    {
        $this->builder = $this->db->table($value['table']);
        $data = array('status' => '1',);

        if (isset($value['field'])) {
            $this->builder->where($value['field'], $value['id']);
        } else {
            $this->builder->where($value['search'], $value['id']);
        }

        $this->builder->set($data);
        $this->builder->update();
        // Set Log
        return ($this->db->affectedRows() > 0) ? TRUE : FALSE;
    }
}
