<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
	public function up()
	{
		$attributes = ['ENGINE' => 'MyISAM'];
		$this->forge->addField([
			'id_user'          => [
				'type'           => 'INT',
				'constraint'     => 11,
				'default'        => NULL,
				'auto_increment' => true,
			],
			'nama_user'       => [
				'type'       => 'VARCHAR',
				'constraint' => '500',
				'default'        => NULL,
			],
			'email'       => [
				'type'       => 'VARCHAR',
				'constraint' => '500',
				'default'        => NULL,
			],
			'password'       => [
				'type'       => 'VARCHAR',
				'constraint' => '500',
				'default'        => NULL,
			],
			'role' => [
				'type' => 'tinyint',
				'constraint'     => 1,
				'default'        => '1',
			],
			'`created_at` datetime default current_timestamp',
			'`created_by` int(11) default NULL',
			'`updated_at` datetime default current_timestamp on update current_timestamp',
			'`updated_by` int(11) default NULL',
			'status' => [
				'type' => 'tinyint',
				'constraint'     => 1,
				'default'        => '1',
			],
		]);
		$this->forge->addKey('id_user', true);
		$this->forge->createTable('m_user', FALSE, $attributes);
	}

	public function down()
	{
		$this->forge->dropTable('m_user');
	}
}
