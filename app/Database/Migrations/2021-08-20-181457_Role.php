<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Role extends Migration
{
	public function up()
	{
		$attributes = ['ENGINE' => 'MyISAM'];
		$this->forge->addField([
			'id_role'          => [
				'type'           => 'INT',
				'constraint'     => 11,
				'default'        => NULL,
				'auto_increment' => true,
			],
			'nama_role'       => [
				'type'       => 'VARCHAR',
				'constraint' => '500',
			],
			'`created_at` datetime default current_timestamp',
			'`updated_at` datetime default current_timestamp on update current_timestamp',
			'status' => [
				'type' => 'tinyint',
				'constraint'     => 1,
				'default'        => '1',
			],
		]);
		$this->forge->addKey('id_role', true);
		$this->forge->createTable('m_role', FALSE, $attributes);
	}

	public function down()
	{
		$this->forge->dropTable('m_role');
	}
}
