<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Menu extends Migration
{
	public function up()
	{
		$attributes = ['ENGINE' => 'MyISAM'];
		$this->forge->addField([
			'id_menu'          => [
				'type'           => 'INT',
				'constraint'     => 11,
				'default'        => NULL,
				'auto_increment' => true,
			],
			'id_parent' => [
				'type' => 'int',
				'constraint'     => 11,
				'default'        => '0',
			],
			'nama_menu'       => [
				'type'       => 'VARCHAR',
				'constraint' => '500',
			],
			'`icon` varchar(555) DEFAULT NULL',
			'`link` varchar(555) DEFAULT NULL',
			'`page` varchar(255) DEFAULT NULL',
			'`header` tinyint(1) DEFAULT 0',
			'`order` int(3) DEFAULT 1',
			'`created_at` datetime default current_timestamp',
			'`updated_at` datetime default current_timestamp on update current_timestamp',
			'status' => [
				'type' => 'tinyint',
				'constraint'     => 1,
				'default'        => '1',
			],
		]);
		$this->forge->addKey('id_menu', true);
		$this->forge->createTable('m_menu', FALSE, $attributes);
	}

	public function down()
	{
		$this->forge->dropTable('m_menu');
	}
}
