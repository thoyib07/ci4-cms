<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Setting extends Migration
{
	public function up()
	{
		$attributes = ['ENGINE' => 'MyISAM'];
		$this->forge->addField([
			'id_setting'          => [
				'type'           => 'INT',
				'constraint'     => 11,
				'default'        => NULL,
				'auto_increment' => true,
			],
			'name_setting'       => [
				'type'       => 'VARCHAR',
				'constraint' => '500',
			],
			'value'       => [
				'type'       => 'VARCHAR',
				'constraint' => '750',
			],
			'created_at datetime default current_timestamp',
			'updated_at datetime default current_timestamp on update current_timestamp',
			'status' => [
				'type' => 'tinyint',
				'constraint'     => 1,
				'default'        => '1',
			],
		]);
		$this->forge->addKey('id_setting', true);
		$this->forge->createTable('m_setting', FALSE, $attributes);
	}

	public function down()
	{
		$this->forge->dropTable('m_setting');
	}
}
