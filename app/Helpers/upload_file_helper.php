<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
include('image_proses_helper.php');

function unggah_berkas($path, $nama_file, $type) {
	$CI = get_instance();
	$CI->load->library('upload');

	if (!file_exists($path)) {
		mkdir($path, 0777, true);
	}

	$config['upload_path'] = $path;
	$config['allowed_types'] = $type;
	$config['max_size']	= '262144';	
	$config['form_file_name'] = $nama_file;
	$config['overwrite'] = TRUE;
	
	$new_name		= time();
	$real_name 		= str_replace([" "],"_",$_FILES[$config['form_file_name']]['name']);
	$new_nama_file 	= $new_name."-".$real_name;

	$config['file_name'] = $new_nama_file;
	// var_dump("<hr> Config Upload <br>",$config);
	$CI->upload->initialize($config);
	if ( ! $CI->upload->do_upload($nama_file)){
		$error = array('error' => $CI->upload->display_errors());
		return "Gagal Upload";
	} else {
		$data = array('upload_data' => $CI->upload->data());
		return $path.'/'.$config['file_name'];
	}
}

function unggah_gambar($path, $nama_file, $type) {
	$CI = get_instance();
	$CI->load->library('upload');

	if (!file_exists($path.'/origin')) {
		mkdir($path.'/origin', 0777, true);
	}

	if (!file_exists($path.'/thumb')) {
		mkdir($path.'/thumb', 0777, true);
	}

	$config['upload_path'] = $path.'/origin';
	$config['allowed_types'] = $type;
	$config['max_size']	= '262144';	
	$config['form_file_name'] = $nama_file;
	$config['overwrite'] = TRUE;
	
	$new_name		= time();
	$real_name 		= str_replace([" "],"_",$_FILES[$config['form_file_name']]['name']);
	$new_nama_file 	= $new_name."-".$real_name;

	$config['file_name'] = $new_nama_file;
	// var_dump("<hr> Config Upload <br>",$config);
	$CI->upload->initialize($config);
	if ($CI->upload->do_upload($nama_file)){
		$data = array('upload_data' => $CI->upload->data());
		$config['source_image'] = $config['upload_path'].'/'.$config['file_name'];
		$config['new_image'] = $path.'/thumb/'.$config['file_name'];
		$config['type'] = 'resize';
		$config['width'] = 500;
		imageProses($config);
		return $config['file_name'];
	} else {
		$error = array('error' => $CI->upload->display_errors(),'status' => false);
		// var_dump("error <hr>",$error);
		return $error;
	}
}

/* End of file upload_file_helper.php */