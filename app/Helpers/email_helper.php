<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function send_email($setting){
	$CI = get_instance();
	// var_dump($data->detail[0]);
	// die();
    $CI->load->library('email');
	if (isset($setting->alias_from)) {
		$CI->email->from($setting->email_from, $setting->alias_from);
	} else {
		$CI->email->from($setting->email_from);
	}

	if (isset($setting->reply_to)) {
		$CI->email->reply_to($setting->email_from, $setting->alias_from);
	}
	
	$CI->email->to($setting->email_to);

	$CI->email->subject($setting->subject);
	$CI->email->message($setting->message);

	if ($CI->email->send(FALSE)) {
		return TRUE;
	} else {
		echo ($CI->email->print_debugger(array('header')));
		return FALSE;
	}
}