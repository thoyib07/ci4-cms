<?php
    function create_pagging($config) {
        $CI = get_instance();
        $CI->load->library('pagination');

        $config['use_page_numbers'] = TRUE;
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';
        $config['next_link'] = '>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';
        $config['prev_link'] = '<';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';
        $config['last_link'] = 'Last';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item disabled"><a class="page-link" href="#" tabindex="-1" aria-disabled="true">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['attributes'] = array('class' => 'page-link');

        $CI->pagination->initialize($config);	

        return '<nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">'
                        .$CI->pagination->create_links().
                    '</ul>
                </nav>';
    }
?>