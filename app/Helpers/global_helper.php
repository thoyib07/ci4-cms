<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function random_code($length = 10,$type='') {
    switch ($type) {
        case 'alphabet':
            $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
        case 'numeric':
            $characters = '0123456789';
            break;
        case 'alphanumeric':
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
        case 'alphanumeric2':
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*?<>-+=';
            break;
        
        default:
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            break;
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function create_link($string,$kanal="") {
	$string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
    $link_gen = time()."-".preg_replace('/[^A-Za-z0-9\_]/', '', $string);// Removes special chars.
    if (!empty($kanal)) {
        return $kanal."/".$link_gen;
    } else {   
        return $link_gen;
    }
}

function date_indonesia($format = 'd F, Y',$timestamp = NULL) {
    $l = array('', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Minggu');
    $F = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

    $return = '';
    if(is_null($timestamp)) { $timestamp = mktime(); }
    for($i = 0, $len = strlen($format); $i < $len; $i++) {
        switch($format[$i]) {
            case '\\' :
                $i++;
                $return .= isset($format[$i]) ? $format[$i] : '';
                break;
            case 'l' :
                $return .= $l[date('N', $timestamp)];
                break;
            case 'F' :
                $return .= $F[date('n', $timestamp)];
                break;
            default :
                $return .= date($format[$i], $timestamp);
                break;
        }
    }
    return $return;
}

function build_menu($menu) {
    // var_dump(json_encode($menu));
    $result = array();
    foreach ($menu as $val) {
        $result[$val->page] = array(
            "nama_menu" => $val->nama_menu,
            "icon" => $val->icon,
            "link" => $val->link,
            "page" => $val->page,
            "header" => $val->header,
            "create" => $val->create,
            "update" => $val->update,
            "delete" => $val->delete,
            "restore" => $val->restore,
        );
    }
    // var_dump(json_encode($result));
    // die;
    return $result;
}

function validate_access($menu) {    
    if (empty($menu)) { redirect('admin'); }
}

function set_notifikasi($data) {
	$CI = get_instance();
    $notifikasi['data'] = $data;
    $notifikasi['table']		= "t_notifikasi";
    $CI->model_global->addData($notifikasi);
}

function get_notifikasi($id_pesreta) {
	$CI = get_instance();
    $notifikasi_detail['select'] = "*";
    $notifikasi_detail['from'] = "t_notifikasi";
    $notifikasi_detail['where'] = "penerima = ".$id_pesreta." AND is_read = 0";
    $notifikasi_detail['order'][] = array('created_at','desc');
    $data_notifikasi = $CI->model_global->getData($notifikasi_detail);
    return $data_notifikasi;
}