<!DOCTYPE html>
<html lang="en">
<?= $this->include('admin/layout/part/head'); ?>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?= $this->include('admin/layout/part/nav'); ?>
        <?= $this->include('admin/layout/part/menu_bar'); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <?= $this->renderSection('content'); ?>
        </div>
        <?= $this->include('admin/layout/part/footer'); ?>
    </div>
    <!-- ./wrapper -->
    <?= $this->include('admin/layout/part/js_footer'); ?>
</body>

</html>